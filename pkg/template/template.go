package template

import (
	"bytes"
	"fmt"
	"html/template"
	"io"
	"os"
	"strings"

	"gitlab.com/jackloughran/site-generator/pkg/generate"
)

type Generator struct {
	Logger                    generate.Logger
	MarkdownToHTMLTransformer generate.DataTransformer
	HTMLMinifier              generate.Minifier
	CSSMinifier               generate.Minifier
	TemplateDir               string
	BuildDir                  string
	CSSPath                   string
	BaseURL                   string
}

var styleCache = make(map[string]template.CSS)

func (g *Generator) Generate(sectionName string, post *generate.Post) error {
	buf, err := processPostName(post)
	if err != nil {
		return fmt.Errorf("could not process %s: %v", post.FileName, err)
	}

	markdownData := buf.Bytes()

	htmlData, err := g.MarkdownToHTMLTransformer.Transform(markdownData)
	if err != nil {
		return fmt.Errorf("error transforming markdown into html for %s: %v", post.Name, err)
	}

	tmpl, err := template.ParseFiles(g.TemplateDir+"/post.html", g.TemplateDir+"/head.html", g.TemplateDir+"/header.html")
	if err != nil {
		return fmt.Errorf("couldn't parse template files: %v", err)
	}

	css, err := g.GetStyle(g.CSSPath)
	if err != nil {
		return fmt.Errorf("couldn't get style for %s: %v", g.CSSPath, err)
	}

	var postPageHTML strings.Builder
	err = tmpl.Execute(&postPageHTML, struct {
		Name    string
		Date    string
		Content template.HTML
		Style   template.CSS
		BaseURL string
	}{
		Name:    post.Name,
		Date:    post.Date.Format("January 2nd, 2006"),
		Content: template.HTML(string(htmlData)),
		Style:   css,
		BaseURL: g.BaseURL,
	})
	if err != nil {
		return fmt.Errorf("could not execute template: %v", err)
	}

	post.URL = sectionName + "/" + post.FileName[:len(post.FileName)-3]

	finishedPost, err := os.Create(g.BuildDir + "/" + post.URL)
	if err != nil {
		return fmt.Errorf("could not create new post file: %v", err)
	}

	var builder strings.Builder

	if err := g.HTMLMinifier.Minify(&builder, strings.NewReader(postPageHTML.String())); err != nil {
		return fmt.Errorf("could not minify html for %s: %v", post.Name, err)
	}

	finishedPost.Write([]byte(builder.String()))

	return nil
}

func processPostName(post *generate.Post) (*bytes.Buffer, error) {
	f, err := os.Open(post.Path)
	if err != nil {
		return nil, fmt.Errorf("couldn't open %s for processing: %v", post.Path, err)
	}

	fi, err := f.Stat()
	if err != nil {
		return nil, fmt.Errorf("couldn't stat %s for processing: %v", post.Path, err)
	}

	buf := bytes.NewBuffer(make([]byte, 0, fi.Size()))
	_, err = f.Seek(0, os.SEEK_SET)
	if err != nil {
		return nil, fmt.Errorf("couldn't seek to the beginning of %s: %v", post.Path, err)
	}

	_, err = io.Copy(buf, f)
	if err != nil {
		return nil, fmt.Errorf("couldn't copy contents of %s into buffer: %v", post.Path, err)
	}

	postName, err := buf.ReadString('\n')
	if err != nil {
		return nil, fmt.Errorf("couldn't read first line of %s: %v", post.Path, err)
	}

	post.Name = postName

	return buf, nil
}

func (g *Generator) GetStyle(path string) (template.CSS, error) {
	if styleCache[path] == "" {
		var builder strings.Builder

		cssFile, err := os.Open(g.CSSPath)
		if err != nil {
			return "", fmt.Errorf("could not open css file %s: %v", g.CSSPath, err)
		}

		if err := g.CSSMinifier.Minify(&builder, cssFile); err != nil {
			return "", fmt.Errorf("could not minify css: %v", err)
		}

		styleCache[path] = template.CSS(builder.String())
	}

	return styleCache[path], nil
}
