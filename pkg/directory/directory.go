// Package directory is going to handle setting up the directories that built
// files will go into.
package directory

import (
	"os"

	"gitlab.com/jackloughran/site-generator/pkg/generate"
)

// Generator is used to generate the directories
type Generator struct {
	DirName string
	Logger  generate.Logger
}

// Generate will clear/create the build directory
func (g *Generator) Generate() error {
	g.Logger.Println("creating fresh directory: " + g.DirName)
	// Check if file exists
	_, err := os.Stat(g.DirName)
	if err == nil {
		g.Logger.Println(g.DirName + " already exists")
		err = os.RemoveAll(g.DirName)
		if err != nil {
			return err
		}
		g.Logger.Println("removed old " + g.DirName)
	}

	return os.Mkdir(g.DirName, 0755)
}
