package log

import (
	"log"
	"os"
)

// Logger statisfies the generate.Logger interface
type Logger struct {
	logger *log.Logger
}

var logfile *os.File

// SetFileOutput sets the global logging output to a file
func SetFileOutput(fileName string) {
	f, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatal("error opening file " + fileName)
	}

	logfile = f
}

// NewLogger will give a logger using the same file but with a custom prefix
func NewLogger(prefix string) *Logger {
	return &Logger{
		logger: log.New(logfile, prefix+": ", log.LstdFlags),
	}
}

// Println calls log.Println
func (l *Logger) Println(v ...interface{}) {
	l.logger.Println(v...)
}

// Printf calls log.Printf
func (l *Logger) Printf(format string, v ...interface{}) {
	l.logger.Printf(format, v...)
}
