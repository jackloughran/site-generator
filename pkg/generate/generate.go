// Package generate contians all the generic (interfaces) information about
// other packages and functions that are actually used to generate the site.
package generate

import (
	"html/template"
	"io"
	"time"
)

type Post struct {
	Name     string
	Date     time.Time
	Path     string
	FileName string
	URL      string
}

// Generator will generate some part of the site
type Generator interface {
	Generate() error
}

// TemplateGenerator is a special generator that generates templates
type TemplateGenerator interface {
	Generate(sectionName string, post *Post) error
	GetStyle(path string) (template.CSS, error)
}

// DataTransformer takes some data and returns some more data
type DataTransformer interface {
	Transform(input []byte) ([]byte, error)
}

// Minifier minifies
type Minifier interface {
	Minify(w io.Writer, r io.Reader) error
}

// Logger can log messages anywhere
type Logger interface {
	Println(v ...interface{})
	Printf(format string, v ...interface{})
}

// Sections holds all the sections and post names
type Sections map[string]*[]Post
