package minify

import (
	"fmt"
	"io"
	"io/ioutil"

	"github.com/tdewolff/minify"
	"github.com/tdewolff/minify/css"
	"github.com/tdewolff/minify/html"
)

// HTMLMinifier minifies html
type HTMLMinifier struct {
	WillMinify bool
}

// CSSMinifier minifies css
type CSSMinifier struct {
	WillMinify bool
}

func (mi *CSSMinifier) Minify(w io.Writer, r io.Reader) error {
	if mi.WillMinify {
		m := minify.New()
		m.AddFunc("text/css", css.Minify)

		return m.Minify("text/css", w, r)
	}
	b, err := ioutil.ReadAll(r)
	if err != nil {
		return fmt.Errorf("unable to read css, non minifying: %v", err)
	}
	_, err = w.Write(b)
	if err != nil {
		return fmt.Errorf("unable to write css, non minifying: %v", err)
	}
	return nil
}

func (mi *HTMLMinifier) Minify(w io.Writer, r io.Reader) error {
	if mi.WillMinify {
		m := minify.New()
		m.AddFunc("text/html", html.Minify)

		return m.Minify("text/html", w, r)
	}

	b, err := ioutil.ReadAll(r)
	if err != nil {
		return fmt.Errorf("unable to read html, non minifying: %v", err)
	}
	_, err = w.Write(b)
	if err != nil {
		return fmt.Errorf("unable to write html, non minifying: %v", err)
	}
	return nil
}
