package sections

import (
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/jackloughran/site-generator/pkg/generate"
)

// Generator will generate the sections and html
type Generator struct {
	Logger                    generate.Logger
	ContentDir                string
	MarkdownToHTMLTransformer generate.DataTransformer
	TemplateDir               string
	BuildDir                  string
	CSSPath                   string
	TemplateGenerator         generate.TemplateGenerator
	BaseURL                   string
}

// Generate will generate html for each of the sections
func (g *Generator) Generate() (*generate.Sections, error) {
	g.Logger.Printf("getting sections via directories from %s", g.ContentDir)

	files, err := ioutil.ReadDir(g.ContentDir)
	if err != nil {
		return nil, fmt.Errorf("unable to read dir %s: %s", g.ContentDir, err.Error())
	}

	var sectionNames []string
	for _, f := range files {
		if f.IsDir() {
			sectionNames = append(sectionNames, f.Name())
		}
	}

	g.Logger.Printf("found sections %v", sectionNames)

	var sectionsByPosts generate.Sections = make(map[string]*[]generate.Post)

	for _, sectionName := range sectionNames {
		posts, err := generateSection(g.ContentDir, sectionName, g.TemplateDir, g.BuildDir, g.CSSPath, g.Logger, g.MarkdownToHTMLTransformer, g.TemplateGenerator)
		if err != nil {
			return nil, fmt.Errorf("could not generate section %s: %v", sectionName, err)
		}

		sectionsByPosts[sectionName] = posts
	}

	return &sectionsByPosts, nil
}

func generateSection(contentDir, sectionName, templateDir, buildDir, cssPath string, logger generate.Logger, markdownToHTMLTransformer generate.DataTransformer, templateGenerator generate.TemplateGenerator) (*[]generate.Post, error) {
	logger.Printf("generating section %s", sectionName)

	sectionDir := contentDir + "/" + sectionName
	sectionBuildDir := buildDir + "/" + sectionName

	err := os.Mkdir(sectionBuildDir, 0755)
	if err != nil {
		return nil, fmt.Errorf("could not make section dir %s: %v", sectionDir, err)
	}

	logger.Printf("created section directory: %s", sectionBuildDir)

	files, err := ioutil.ReadDir(sectionDir)
	if err != nil {
		return nil, fmt.Errorf("unable to read dir %s for section generation: %v", sectionDir, err)
	}

	var posts []generate.Post
	for _, f := range files {
		if f.IsDir() {
			return nil, fmt.Errorf("found directory %s in %s. only files are allowed in sections", f.Name(), sectionDir)
		}

		posts = append(posts, generate.Post{
			Date:     f.ModTime(),
			Path:     sectionDir + "/" + f.Name(),
			FileName: f.Name(),
		})
	}

	logger.Printf("found posts %v in section %s", posts, sectionName)

	for i := range posts {
		// cannot grab a copy of the post element here since i want to modify
		// the post inside the generate method.
		//
		// i need this because im storing data at the top of the file and the
		// logic to grab that data should live with the rest of the logic to
		// process the file contents.
		//
		// post.Name, and post.URL are currently populated inside
		// templateGenerator.Generate()
		err = templateGenerator.Generate(sectionName, &posts[i])
		if err != nil {
			return nil, fmt.Errorf("could not generate post %v: %v", posts[i], err)
		}
	}

	return &posts, nil
}
