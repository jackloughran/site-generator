package markdown

import (
	"github.com/microcosm-cc/bluemonday"
	"gopkg.in/russross/blackfriday.v2"
)

// ToHTMLTransformer transforms markdown to html
type ToHTMLTransformer struct{}

// Transform generates html given markdown data
func (t *ToHTMLTransformer) Transform(input []byte) ([]byte, error) {
	html := blackfriday.Run(input)
	html = bluemonday.UGCPolicy().SanitizeBytes(html)

	return html, nil
}
