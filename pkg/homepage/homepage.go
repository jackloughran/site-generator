package homepage

import (
	"fmt"
	"html/template"
	"os"
	"strings"

	"gitlab.com/jackloughran/site-generator/pkg/generate"
)

// Generator will be used to generate the homepage
type Generator struct {
	BuildDir          string
	ContentDir        string
	TemplateDir       string
	Logger            generate.Logger
	TemplateGenerator generate.TemplateGenerator
	CSSPath           string
	HTMLMinifier      generate.Minifier
	SectionsByPosts   *generate.Sections
	BaseURL           string
}

// Generate will generate a homepage in the build dir
func (g *Generator) Generate() error {
	g.Logger.Println("opening " + g.ContentDir + "/index.html for writing homepage")
	file, err := os.OpenFile(g.BuildDir+"/index.html", os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return fmt.Errorf("error opening file to write homepage to: " + err.Error())
	}

	t, err := template.ParseFiles(g.ContentDir+"/index.html", g.TemplateDir+"/head.html", g.TemplateDir+"/header.html")
	if err != nil {
		return fmt.Errorf("error parsing the template for the homepage: " + err.Error())
	}

	css, err := g.TemplateGenerator.GetStyle(g.CSSPath)
	if err != nil {
		return fmt.Errorf("could not get the style for the homepage: %v", err)
	}

	sectionsMap := map[string]*[]generate.Post(*g.SectionsByPosts)

	var homepageHTML strings.Builder
	g.Logger.Println("writing to " + g.ContentDir + "/index.html from template file " + g.BuildDir + "/index.html")
	err = t.Execute(&homepageHTML, struct {
		Style   template.CSS
		Books   []generate.Post
		Notes   []generate.Post
		BaseURL string
	}{
		Style:   css,
		Books:   *sectionsMap["books"],
		Notes:   *sectionsMap["notes"],
		BaseURL: g.BaseURL,
	})
	if err != nil {
		return fmt.Errorf("error executing the template for the homepage: " + err.Error())
	}

	var minifiedBuilder strings.Builder
	if err := g.HTMLMinifier.Minify(&minifiedBuilder, strings.NewReader(homepageHTML.String())); err != nil {
		return fmt.Errorf("could not minify homepage html: %v", err)
	}

	file.Write([]byte(minifiedBuilder.String()))

	return nil
}
