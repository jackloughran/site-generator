package main

import (
	"flag"
	"fmt"

	"gitlab.com/jackloughran/site-generator/pkg/directory"
	"gitlab.com/jackloughran/site-generator/pkg/homepage"
	"gitlab.com/jackloughran/site-generator/pkg/log"
	"gitlab.com/jackloughran/site-generator/pkg/markdown"
	"gitlab.com/jackloughran/site-generator/pkg/minify"
	"gitlab.com/jackloughran/site-generator/pkg/sections"
	"gitlab.com/jackloughran/site-generator/pkg/template"
)

var noMinify = flag.Bool("m", false, "no minify: setting this to true will stop css and html minification.")
var baseURL = flag.String("u", "https://jloughran.com", "set the base url. defaults to https://jloughran.com")

const (
	buildDir    = "build"
	templateDir = "templates"
	contentDir  = "content"
	cssPath     = templateDir + "/base.css"
)

func main() {
	flag.Parse()

	fmt.Println("jloughran.com Site Generator")
	fmt.Println("----------------------------\n\n")

	log.SetFileOutput("generate.log")

	log.NewLogger("----root").Println("generation started")

	directoryGenerator := directory.Generator{
		DirName: buildDir,
		Logger:  log.NewLogger("directoryGenerator"),
	}

	fmt.Println("creating build directory\n")
	err := directoryGenerator.Generate()
	if err != nil {
		panic(err)
	}

	htmlMinifier := &minify.HTMLMinifier{WillMinify: !*noMinify}
	cssMinifier := &minify.CSSMinifier{WillMinify: !*noMinify}

	markdownToHTMLTransformer := &markdown.ToHTMLTransformer{}

	templateGenerator := &template.Generator{
		Logger: log.NewLogger("templateGenerator"),
		MarkdownToHTMLTransformer: markdownToHTMLTransformer,
		HTMLMinifier:              htmlMinifier,
		CSSMinifier:               cssMinifier,
		TemplateDir:               templateDir,
		BuildDir:                  buildDir,
		CSSPath:                   cssPath,
		BaseURL:                   *baseURL,
	}

	fmt.Println("next let's generate the sections\n")
	sectionsGenerator := sections.Generator{
		ContentDir: contentDir,
		Logger:     log.NewLogger("sectionsGenerator"),
		MarkdownToHTMLTransformer: markdownToHTMLTransformer,
		TemplateDir:               templateDir,
		BuildDir:                  buildDir,
		CSSPath:                   cssPath,
		TemplateGenerator:         templateGenerator,
	}
	sectionsByPosts, err := sectionsGenerator.Generate()
	if err != nil {
		panic(err)
	}

	fmt.Println("now let's make the homepage\n")
	homepageGenerator := homepage.Generator{
		BuildDir:          buildDir,
		ContentDir:        contentDir,
		Logger:            log.NewLogger("homepageGenerator"),
		TemplateGenerator: templateGenerator,
		CSSPath:           cssPath,
		HTMLMinifier:      htmlMinifier,
		SectionsByPosts:   sectionsByPosts,
		TemplateDir:       templateDir,
		BaseURL:           *baseURL,
	}
	err = homepageGenerator.Generate()
	if err != nil {
		panic(err)
	}
}
