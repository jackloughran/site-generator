VERSION := $(shell cat ./VERSION)

build: clean
	go build ./cmd/generate/generate.go

run:
	go run ./cmd/generate/generate.go

clean:
	rm -rf generate.log
	rm -rf build
	rm -rf generate

debug:
	dlv debug ./cmd/generate/generate.go